
package com.example.vguizilini.testflow;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String MODEL_FILE  = "file:///android_asset/optimized_tfdroid.pb";
    private static final String INPUT_NODE  = "I";
    private static final String OUTPUT_NODE = "O";
    private static final int[]  INPUT_SIZE  = {1,3};

    private static final int RESULT_LOAD_IMAGE = 1;

    private TensorFlowInferenceInterface inferenceInterface;

    static
    {
        System.loadLibrary( "tensorflow_inference" );
    }

    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText et1 = (EditText) findViewById( R.id.editText1 );
        final EditText et2 = (EditText) findViewById( R.id.editText2 );
        final EditText et3 = (EditText) findViewById( R.id.editText3 );

        final TextView to = (TextView) findViewById( R.id.textOutput );

        final Button buttonCalc = (Button) findViewById( R.id.buttonCalc );
        final Button buttonLoad = (Button) findViewById( R.id.buttonLoad );
        final Button buttonSave = (Button) findViewById( R.id.buttonSave );

        inferenceInterface = new TensorFlowInferenceInterface();
        inferenceInterface.initializeTensorFlow( getAssets() , MODEL_FILE );

        buttonCalc.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                float num1 = Float.parseFloat( et1.getText().toString() );
                float num2 = Float.parseFloat( et2.getText().toString() );
                float num3 = Float.parseFloat( et3.getText().toString() );

                float[] inputsFloat  = { num1 , num2 , num3 };
                float[] outputsFloat = { 0 , 0 };

                inferenceInterface.fillNodeFloat( INPUT_NODE , INPUT_SIZE , inputsFloat );
                inferenceInterface.runInference( new String[] { OUTPUT_NODE } );
                inferenceInterface.readNodeFloat( OUTPUT_NODE , outputsFloat );

                to.setText( Float.toString( outputsFloat[0] ) + " , " + Float.toString( outputsFloat[1] ) );
            }
        });

        buttonLoad.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent i = new Intent( Intent.ACTION_PICK ,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI );
                startActivityForResult( i , RESULT_LOAD_IMAGE );
            }
        });

        buttonSave.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                ImageView imageOutput = (ImageView)findViewById( R.id.imageOutput );
                Bitmap output = ((BitmapDrawable)imageOutput.getDrawable()).getBitmap();
                saveImage( output );

            }
        });
    }

    @Override
    protected void onActivityResult( int requestCode , int resultCode , Intent data )
    {
        super.onActivityResult( requestCode , resultCode , data );

        if( requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null )
        {
            Uri selectedImage = data.getData();
            ImageView imageLoaded = (ImageView)findViewById( R.id.imageLoaded );
            ImageView imageOutput = (ImageView)findViewById( R.id.imageOutput );
            imageLoaded.setImageURI( selectedImage );

            final TextView to = (TextView) findViewById( R.id.textOutput );
            to.setText( path );

            try
            {
                Bitmap input = MediaStore.Images.Media.getBitmap( getContentResolver() , selectedImage );

                int w = input.getWidth();
                int h = input.getHeight();

                int[] pixels = new int[ w * h ];
                input.getPixels( pixels , 0 , w , 0 , 0 , w , h );

                Bitmap output = Bitmap.createBitmap( pixels , 0 , w , w , h , input.getConfig() );
                imageOutput.setImageBitmap( output );
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }


    private void saveImage(Bitmap finalBitmap)
    {
//        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString();
//        File myDir = new File(root + "/");

//        File myDir = new File("/sdcard/DCIM/Camera");

//        String pathtoimage =  Environment.getExternalStorageDirectory().getAbsolutePath();
//        File myDir = new File( pathtoimage + "/saved_images/");

        File myDir = new File(Environment.getExternalStorageDirectory(),"aaa");

//        String path = getFilesDir().getAbsolutePath();
//        File myDir = new File( path );

        myDir.mkdirs();
        Random generator = new Random();

        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        file.getParentFile().mkdirs();

        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            Toast.makeText( MainActivity.this , out.toString() + " " + file.toString() , Toast.LENGTH_LONG ).show();
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            Toast.makeText( MainActivity.this , "2222" + file.toString() , Toast.LENGTH_LONG ).show();
            out.flush();
            out.close();

        }
        catch (Exception e) {
            e.printStackTrace();
        }


        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(this, new String[] { file.toString() }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });

    }




//    private void saveImage( Bitmap bitmap )
//    {
//        String path = null;
//        FileOutputStream out = null;
//        File file = null;
//
//        try {
//
//            path = Environment.getExternalStorageDirectory().toString();
//            file = new File( path , "/FitnessGir.png" );
////            file.createNewFile(); // if file already exists will do nothing
//
//            out = new FileOutputStream( file , false );
////            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
//            // PNG is a lossless format, the compression factor (100) is ignored
//        } catch (FileNotFoundException e) {
//            Toast.makeText( MainActivity.this , file.toString() , Toast.LENGTH_LONG ).show();
//            e.printStackTrace();
//        } finally {
//            try {
//                Toast.makeText( MainActivity.this , "Error2" , Toast.LENGTH_LONG ).show();
//                if (out != null) {
//                    out.close();
//                }
//            } catch (IOException e) {
//                Toast.makeText( MainActivity.this , "Error3" , Toast.LENGTH_LONG ).show();
//                e.printStackTrace();
//            }
//        }
//
//        Toast.makeText( MainActivity.this , "Image Saved Successfully " + isExternalStorageWritable() , Toast.LENGTH_LONG ).show();
//    }
//
//
//    public boolean isExternalStorageWritable()
//    {
//        String state = Environment.getExternalStorageState();
//        if ( Environment.MEDIA_MOUNTED.equals( state ) )
//        {
//            return true;
//        }
//        return false;
//    }


}

